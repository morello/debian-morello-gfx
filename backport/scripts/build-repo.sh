#!/bin/bash -e

function init_dir ()
{
  if [[ -d "$1" ]]; then
    echo "$1 exists"
  else
    mkdir "$1"
  fi
}

apt-get install \
  reprepro

init_dir ./repo
init_dir ./repo/apt
init_dir ./repo/apt/debian
init_dir ./repo/apt/debian/conf

echo \
'Origin: Morello
Label: Morello
Codename: bullseye
Architectures: source arm64
Components: main
UDebComponents: main
Description: Extra backport packages for Bullseye for Morello' > \
./repo/apt/debian/conf/distributions

echo \
'basedir ./' > \
./repo/apt/debian/conf/options

cd './repo/apt/debian'

for I in ../../../backport/pk/*.changes; do
  reprepro --ignore=wrongdistribution include bullseye $I
done



#!/bin/bash -e

# Where to get unstable dsc sources
DHST="http://cdn-fastly.deb.debian.org"
DPOL="debian/pool"
URL="$DHST/$DPOL"

export MESON_CI_JOBNAME=thirdparty

function mv_pk_if_exists ()
{
  for F in $@; do
    if [[ -f $F ]]; then
      echo "mv "$F" ..."
      mv $F ./pk/
    fi
  done
}

function handle_error ()
{
  RET="$1"
  if test "$RET" -ne 0; then
    tail -10000 ../build-log
    echo "=============================="
    echo "------------------------------"
    echo "ERROR: BUILD FAILED. See above"
    echo "------------------------------"
    echo "=============================="
    exit $RET
  fi
}

function bpk ()
{
  DIR="$1"
  shift
  SRC="$1"
  shift
  PRE="$@"
  rm -rf "$DIR"
  dget -x "$URL/$SRC.dsc"
  cd "$DIR"
  if [[ -n "$PRE" ]]; then
    echo -- "PRE: $PRE"
    bash -cxe "$PRE"
  fi
  echo "------------------------------"
  echo "Setting up build deps for $DIR"
  echo "------------------------------"
  /bin/echo y | mk-build-deps -ir
  rm -f *.buildinfo *.changes
  echo "-------------------------------------"
  echo "Building $DIR - this may take a while"
  echo "-------------------------------------"
  set +e
  dpkg-buildpackage -us -uc -sa >& ../build-log
  RET=$?
  handle_error $RET
  set -e
  cd ..
  dpkg -i *.deb
  mv_pk_if_exists *.deb
  mv_pk_if_exists *.udeb
  mv_pk_if_exists *.dsc
  mv_pk_if_exists *.tar.*
  mv_pk_if_exists *.buildinfo
  mv_pk_if_exists *.changes
  mv_pk_if_exists *.diff.*
  rm -rf "$DIR"
}

function init_dir ()
{
  if [[ -d "$1" ]]; then
    echo "Init dir: $1 exists"
  else
    echo "Init dir: making $1"
    mkdir "$1"
  fi
}

# Init dirs needed
init_dir ./backport
cd ./backport
init_dir ./pk

# Packages needed to begin building things starting from a new debootstrap
echo "--------------------------------------------"
echo "Setting up base build dependency packages..."
echo "--------------------------------------------"
apt-get update
apt-get -y install \
  wget \
  devscripts \
  libobjc-10-dev \
  python3-yaml \
  wasi-libc \
  equivs
echo "----"
echo "DONE"
echo "----"

###############################################################################
# Source packages to rebuild coming from unstable - versions will need
# updating over time along with any fixup/patchng commands
###############################################################################

# Latest keyring for PGP signature checks - need to keep up in case
KR="debian-keyring_2022.12.24_all.deb"
wget "$URL/main/d/debian-keyring/$KR"
echo "-----------------------------"
echo "Getting up-to-date keyring..."
echo "-----------------------------"
dpkg -i "$KR"
rm -f "$KR"
echo "----"
echo "DONE"
echo "----"

echo "----------------------"
echo "Begin the big build..."
echo "----------------------"
# Build all the packages - these will update versions over time because
# upstream debian will remove older versions agressively and thus break the
# fetch of the upstream files so be prepared for some of these to break at
# random and then have to poke around in $URL and look to see what changed
bpk libdrm-2.4.114                  main/libd/libdrm/libdrm_2.4.114-1
bpk wayland-1.21.0                  main/w/wayland/wayland_1.21.0-1
bpk wayland-protocols-1.31          main/w/wayland-protocols/wayland-protocols_1.31-1
bpk directx-headers-1.606.4         main/d/directx-headers/directx-headers_1.606.4-1
bpk llvm-toolchain-15-15.0.7        main/l/llvm-toolchain-15/llvm-toolchain-15_15.0.7-1 \
  sed  -i \'s:STAGE_ALL_CMAKE_EXTRA\ =:STAGE_ALL_CMAKE_EXTRA\ \=\ -DLLDB_PYTHON_EXE_RELATIVE_PATH=./python3:g\' debian/rules
bpk pkg-kde-tools-0.15.38           main/p/pkg-kde-tools/pkg-kde-tools_0.15.38
bpk spirv-headers-1.6.1+1.3.239.0   main/s/spirv-headers/spirv-headers_1.6.1+1.3.239.0-1
bpk spirv-tools-2023.1              main/s/spirv-tools/spirv-tools_2023.1-2
bpk spirv-llvm-translator-15-15.0.0 main/s/spirv-llvm-translator-15/spirv-llvm-translator-15_15.0.0-2 \
  mv debian/libllvmspirvlib15.symbols debian/libllvmspirvlib15.NOUSE-symbols \; \
  sed  -i \'s/override_dh_auto_test:/ignore_test_failures = -\\noverride_dh_auto_test:/g\' debian/rules
bpk meson-1.0.1                     main/m/meson/meson_1.0.1-5 \
  sed -i s/libwxgtk3.2-dev/libwxgtk3.0-gtk3-dev/g debian/control
bpk mesa-23.1.2                      main/m/mesa/mesa_23.1.2-1 \
  sed -i \'s/RUSTICL_ARCHS\ \ = amd64 arm64 armel armhf/RUSTICL_ARCHS\ \ = amd64/g\' debian/rules
echo "----"
echo "DONE"
echo "----"

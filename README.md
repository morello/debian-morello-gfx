
# Debian Bullseye backports for up to date graphics packages

This will bootstrap a small build setup and build backported packages
for Debian bullseye (stable) release from unstable to pull in all the
fixes and support for bifrost and the panfrost drivers.

## Docker build

If you do not already have docker and docker-compose installed, make
sure it's installed. Follow whatever instructions are relevant for
your OS, but for example

```
apt install docker-compose docker
```

Then you can simply use the docker build script:

```
./build.sh
```

You will find all the resulting packages in ./backport/pk/

----

## Chroot build

You need to start with a new debian chroot on an existing Arm64 host:

[Debian Wiki about chroots](https://wiki.debian.org/chroot)

```sh
# This is where we will build the chroot - customize as needed
R=/mnt/chroot
mkdir $R

# Create the chroot
debootstrap bullseye $R http://deb.debian.org/debian/
# Mount some of the host filesystems in it
mount --bind /proc $R/proc
mount --bind /dev $R/dev
mount --bind /sys $R/sys
mount --bind /run $R/run

# Clone this repo into the chroot
git clone https://git.morello-project.org/morello/debian-morello-gfx.git

# Go into the chroot
chroot $R /bin/bash

# This is where you start running commands inside the chroot
cd /root/deb-gfx/backport
# Build the backport packages
./scripts/build-pkgs.sh
# On success the output packages will be in the ./pk/ directory
# Build a debian repository for these packages to host on a server
./scripts/build-repo.sh
# On success the repository tree will be in the ./repo/ directory

# Exit the chroot shell
exit

# Clean up mounts
umount -R $R/proc $R/dev $R/sys $R/run
```

----

## Latest packages

 * [Latest packageset zip](https://git.morello-project.org/morello/debian-morello-gfx/-/jobs/artifacts/master/download?job=build)

----

## Install packags and get a desktop environment

On your newly installed morello system:

```sh
# Install tools to fetch and unpack packages
apt install zip wget
# Get the latest packages
wget 'https://git.morello-project.org/morello/debian-morello-gfx/-/jobs/artifacts/master/download?job=build' -O packages.zip
# Unzip and install packages
unzip packages.zip
dpkg -i packages/*.deb

# Install basic core X11 + basic login manager
apt install slim xserver-xorg-input-libinput xserver-xorg xterm dbus-x11
# Install some apps to use
apt install firefox-esr openarena
# Install a composited desktop that will use GPU for rendering
apt install enlightenment terminology

# Start and enable login manager so we can log-in in all our graphical glory
systemctl enable slim --now
```
